import React,{Component} from 'react';
import './App.css';
import {Route,Switch} from 'react-router-dom'



import Books from './containers/Products/Books/Books'
import Default from './containers/Products/Default/Default'
import Bags from './containers/Products/Bags/Bags'
import Mics from './containers/Products/Mics/Mics'
import Tshirts from './containers/Products/Tshirts/Tshirts'

class App extends Component{
  state={
  }

  render() {
    return (
        <div className="App">
            <Switch>
              <Route path='/Books'  component={Books}/>
              <Route path='/Bags'  component={Bags}/>
              <Route path='/Mics' component={Mics}/>
              <Route path='/Tshirts' component={Tshirts}/>
              <Route path='/' exact component={Default}/>
            </Switch>

        </div>
    );
  }
}


export default App;
